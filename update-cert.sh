#!/bin/sh

# script-hook to upload letencrypt certificates

set -e
#set -x

unset CERDIR
unset TARGET
CONFIG=/root/.update-cert.cfg

TEMP=`getopt -o hdtc --long help,certificate-directory,target,config \
     -n 'update-cert.sh' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
while true ; do
    case "$1" in
        -d|--certificate-directory) CERDIR="$1" ; shift 2;;
        -t|--target) TARGET="$1";  shift 2;;
	-c|--config) CONFIG="$1"; shift 2;;
        -h|--help)
            cat << EOF
Usage:
$0 [-d] [-t]

-d, --certificate-directory <directory>  Certificate directory name (first available in /etc/letsencrypt/live, if absent)
-t, --target '<target1>[:port][ target2[:port]]'       Target hosts, space separared, will be appended to certificate name, =certificate-directory if absent
-c, --config <file>                      File with configuration
-h, --help                               Print help and exit.

For example:
$0 -d www.domain.org -t imap.domain.org
EOF
            exit 0;
            shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [ -r "$CONFIG" ]; then
  . "$CONFIG"
fi


if [ -z "$CERDIR" ]; then
  CERDIR="$(find /etc/letsencrypt/live -mindepth 1 -maxdepth 1 -type d | head -n 1)"
  if [ -z "$CERDIR" ]; then
    echo "Can not determine domain name, failing..." >&2
    exit 101
  fi
fi

if [ -z "$TARGET" ]; then
  TARGET="$(basename "$CERDIR")"
fi

for ii in $TARGET; do
  TARGETHOST="$(echo "$ii" | cut -d ":" -f 1)"
  TARGETPORT="$(echo "$ii" | cut -d ":" -s -f 2)"
  unset PORTOPT
  if [ -n "$TARGETPORT" ]; then
    PORTOPT="-P ${TARGETPORT}"
  fi
  
  SUF="-${TARGETHOST}"
  sftp $PORTOPT sftpuploader@"${TARGETHOST}":writable 2>&1 << EOF
put "$CERDIR"/privkey.pem .privkey${SUF}.pem
rename .privkey${SUF}.pem privkey${SUF}.pem
chmod 0600 privkey${SUF}.pem
put "$CERDIR"/fullchain.pem .fullchain${SUF}.pem
rename .fullchain${SUF}.pem fullchain${SUF}.pem
chmod 0600 fullchain${SUF}.pem
EOF
  done

# other side should copy or link certificates to apropriate place
# and restart/reload services
