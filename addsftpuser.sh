#!/bin/sh

set -e

if [ -n "$1"  ]; then
  adduser --disabled-password "$1"
  usermod -G sftp_users "$1"
fi

cd /home
for ii in *; do
  if (groups "$ii" | grep -wq sftp_users)
  then
    chown root "$ii"
    chmod go-w "$ii"
    chmod g+r "$ii"
    mkdir -p ./"$ii"/writable
    chown "$ii":sftp_users "$ii"/writable
    chmod "ug+rwX" "$ii"/writable
    mkdir -p ./"$ii"/.ssh
    touch ./"$ii"/.ssh/authorized_keys
    chmod go-wrx ./"$ii"/.ssh
    chmod go-wrx ./"$ii"/.ssh/authorized_keys
    chown "$ii":"$ii" ./"$ii"/.ssh -R
  fi
done
