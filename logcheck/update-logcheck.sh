##!/bin/sh

if [ "$(id -un)" = "root" ]; then
  cd /root/scriptadm && git pull --rebase && cp /root/scriptadm/logcheck/extlogignore /etc/logcheck/ignore.d.server
elif [ "$(id -un)" = "lxcuser" ]; then
  cd
  ./lxc-helper/lxcforeach.sh sh -c 'cd /root/scriptadm && git pull --rebase && cp /root/scriptadm/logcheck/extlogignore /etc/logcheck/ignore.d.server'
fi
