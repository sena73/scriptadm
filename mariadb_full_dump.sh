#!/bin/bash
#       Script is expected to run once a day.
#       that will create full dump every day!
#       better use incremental backup!
#       also sometimes does not work for big tables!

export LANG=C

backup_prefix=mysql_dump
backup_dir=/srv/mysql-backup

backup_user=root
#backup_password=should be taken from password option of .my.cnf [client] section

mysqldump_cmd=/usr/bin/mariadb-dump
mysqladmin_cmd=/usr/bin/mariadb-admin
#zip_cmd="xz"
zip_cmd='zstd -22 --ultra --long -T5'
zip_ext="xz"
DEEPEST=5

for kk in $(seq "$DEEPEST" -1 2); do
  pkk=$((kk - 1))
  if [ -r "$backup_dir"/"$backup_prefix"."$pkk"."$zip_ext" ]; then
    mv "$backup_dir"/"$backup_prefix"."$pkk"."$zip_ext" "$backup_dir"/"$backup_prefix"."$kk"."$zip_ext"
  fi
done


DUMP_FNAME="$backup_dir"/"$backup_prefix"."1"."$zip_ext"


${mysqldump_cmd} --quick --max_allowed_packet=512M --compress=TRUE --all-databases --add-drop-table --lock-all-tables --opt -u ${backup_user} | \
		${zip_cmd} > ${DUMP_FNAME} || \
		( echo "Unable to make full dump of database!" && exit 1)

chmod go-r ${DUMP_FNAME}

