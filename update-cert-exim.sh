#!/bin/sh

set -e

# example smtp.domain.org
unset MAINHOST
# example "smtp2.domain.org smtp3.domain.org"
unset SNIHOST
# example /home/sftp/writable
unset SNIDIR
# privkey-
unset KEYPREFIX
# .pem
unset KEYSUFFIX
# fullchain-
unset CRTPREFIX
# .pem
unset CRTSUFFIX

if [ -r /root/.update-cert-exim.cfg ]; then
  . /root/.update-cert-exim.cfg
else
  echo "Can not open config /root/.update-cert-exim.cfg, exiting" >&2	
  exit 101
fi

for ii in $MAINHOST $SNIHOST; do
 KEYNAME="$IMPORTDIR"/"$KEYPREFIX""$ii""$KEYSUFFIX"
 CRTNAME="$IMPORTDIR"/"$CRTPREFIX""$ii""$CRTSUFFIX"
 if [ "$ii" = "$MAINHOST" ]; then
   TARGETNAME=exim
 else
   TARGETNAME="$ii"
 fi

 if [ -r "$CRTNAME" ] &&  [ -r "$KEYNAME" ]; then
  cp "$CRTNAME"   /etc/exim4/"$TARGETNAME".crt
  chown root:root /etc/exim4/"$TARGETNAME".crt
  chmod 644       /etc/exim4/"$TARGETNAME".crt
  cp "$KEYNAME"          /etc/exim4/"$TARGETNAME".key
  chown Debian-exim:root /etc/exim4/"$TARGETNAME".key
  chmod 600              /etc/exim4/"$TARGETNAME".key

  # and restart exim4
  systemctl restart exim4
 else
  echo "Can not open files $CRTNAME $KEYNAME" >&2
  exit 102
 fi
done
