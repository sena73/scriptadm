#!/bin/bash
#       inspired by mysql_backup.sh (2006/09/04) by Danil V. Gerun (danil@sochiwater.ru)
#
#	You have to define at least 5 variables: WHERE_TO_BACKUP, USER,
#	WHERE_YOUR_BINLOGS_RESIDE and YOUR_BINLOGS_INDEX_FILE.
#     
#       If directories are symbolic links, name must be followed by '/'.
#
#	I strongly recommend that you look through this script and do a
#	thorough testing in your environment before doing any rm's.
#
#	It is also a good idea to have a separate MySQL-user for backups.
#	He must be given global permissions of SELECT, RELOAD, FILE, LOCK TABLES.
#       sena: for some reason this does not work for me (created backup user, but 
#       got almost empty db dump).
#
#	The policy of removing old archives can be changed as you prefer.
#
#       Script is expected to run once a day.

export LANG=C

full_backup_suffix=_dump
backup_dir=/srv/mysql-backup

backup_user=root
#backup_password=should be taken from password option of .my.cnf [client] section

mysqldump_cmd=/usr/bin/mysqldump
mysqladmin_cmd=/usr/bin/mysqladmin
zip_cmd="bzip2"

binlogs_dir=/srv/mysql-binlog
binlogs_index=${binlogs_dir}/mariadb-bin.index

#mask for finding the old archived binlogs to delete
delete_find_binlog="-bin"

#mask for finding the old archived dumps to delete
delete_find_dump=${full_backup_suffix}

# date prefix
date_prefix=`date +%Y_%m_%d_%H_%M`

#if time is equal to predefained or $1=dump it will produce full db dump
if [ `date +%A` == "Saturday" -o  "$1" == "dump" ]
then
  DUMP_FNAME=${date_prefix}${full_backup_suffix}
  echo "Full mysql database dump started"
  ${mysqldump_cmd} --all-databases --flush-logs --delete-master-logs --add-drop-table --lock-all-tables --opt -u ${backup_user} | \
		${zip_cmd} > ${backup_dir}/${DUMP_FNAME} || \
		( echo "Unable to make full dump of database!" && exit 1)
  echo "Full dump succeeded, deleting old backup files except the last one"
  for file in `find ${backup_dir} -type f -! -name ${DUMP_FNAME}`
  do
    echo "deleting file ${file}"
    rm ${file}
  done
else
  echo "Starting new bin log"
  ${mysqladmin_cmd} flush-logs -u ${backup_user} || \
		(echo "Unable to rotate bin-logs with mysqladmin!" && exit 1)
fi


#get current binlog name
if current_log=`cat ${binlogs_index} | tail -n 1`
then
  :
else
  echo "Couldn't find the last binlog filename!"
  exit 1
fi

#and check it
if current_log=`echo ${current_log} | grep -e '^[a-z0-9./-]*$' -i`
then
  :
else
  echo "Bad filename of last log!"
  exit 1
fi


#get list of all binlogs for archiving (except for current)
binlogs=`find ${binlogs_dir} -type f -! -path ${current_log} | grep -i -e '^[a-z0-9./-]\+[0-9]\+$' `
for binlog in ${binlogs}
do
  #get basename of binlog
  if bblog=`basename ${binlog}`
  then
    if [ -n "${bblog}" ]
    then
      :
    else
      echo "Can not get basename for ${binlog}, exiting"
      exit 1
    fi
  else
    echo "Error extracting base filename from '${binlog}'!"
    exit 1
  fi

  #archive the binlog
  if cat ${binlog} | ${zip_cmd} > ${backup_dir}/${date_prefix}_${bblog}
  then
    rm ${binlog}
  else
    echo "Couldn't archive file '${binlog}'!"
    exit 1
  fi
done

echo "backup succeeded"
