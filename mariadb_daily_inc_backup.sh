#!/bin/bash
#       Script is expected to run once a day.

set -e

export LANG=en_GB.UTF-8

backup_prefix=mariabackup
backup_base_dir=/srv/mysql-backup
backup_dir="${backup_base_dir}/${backup_prefix}"
create_tar_archive=0

backup_user=root
#backup_password=should be taken from password option of .my.cnf [client] section

#mysqldump_cmd=/usr/bin/mariadb-dump
mysqlbackup_cmd=/usr/bin/mariadb-backup
#mysqladmin_cmd=/usr/bin/mariadb-admin
#zip_cmd="xz"
zip_cmd='zstd -22 --ultra --long -T5'
zip_ext="zst"
# create full backup next Saturday after $NORMAL incremental backups or after $DEEPEST backups
NORMAL=30
DEEPEST=60

# detect current stage
unset CURSTAGE
unset INCBASE

for kk in $(seq "$DEEPEST"); do
  if [ ! -d "${backup_dir}/${kk}" ]; then
    CURSTAGE=${kk}
    break
  else
    INCBASE=${kk}
  fi
done

if [ -z "$CURSTAGE" ] || [ "$CURSTAGE" -ge "$NORMAL" -a "$(date +%A)" = "Saturday" ]; then
  # time to start new full backup now
  # create reserve copy of the last backup
  if [ "$create_tar_archive" = "1" ]; then
    tar -I "${zip_cmd}" --create --file ${backup_dir}.tar.${zip_ext} ${backup_dir}
    chmod go-r ${backup_dir}.tar.${zip_ext}
  else
    if [ -d "${backup_dir}".1 ]; then
      rm -rf "${backup_dir}".1
    fi
    mv "${backup_dir}" "${backup_dir}".1
  fi
  CURSTAGE=1
fi

mkdir -p ${backup_dir}
echo "$CURSTAGE started on $(date --rfc-3339=seconds)" >> ${backup_dir}/completed_log.txt
if [ "$CURSTAGE" = "1" ]; then
  # start new full backup
  ${mysqlbackup_cmd}  -u ${backup_user} --backup --target-dir=${backup_dir}/${CURSTAGE} >${backup_dir}/${CURSTAGE}.log 2>&1
else
  ${mysqlbackup_cmd}  -u ${backup_user} --backup --target-dir=${backup_dir}/${CURSTAGE} --incremental-basedir=${backup_dir}/${INCBASE} >${backup_dir}/${CURSTAGE}.log 2>&1
fi
echo "$CURSTAGE finished on $(date --rfc-3339=seconds)" >> ${backup_dir}/completed_log.txt

# delete reserve copy as we already have new full backup, if any
if [ -d "${backup_dir}".1 ]; then 
  rm -rf "${backup_dir}".1
fi
if [ -e "${backup_dir}".tar."${zip_ext}" ]; then 
  rm -f "${backup_dir}".tar."${zip_ext}"
fi


