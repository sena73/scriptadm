#!/bin/sh

if ! /usr/bin/lsof -Pi :3310 -sTCP:LISTEN -t >/dev/null ; then
  echo "clamav websocket is not running, restarting..."
  /usr/bin/systemctl restart clamav-daemon.socket
fi
